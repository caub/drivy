const assert = require('assert');
const differenceInDays = require('date-fns/difference_in_days'); // to take care of daylight switches
const data = require('./data.json');
const output = require('./output.json');

const cars = new Map(data.cars.map(car => [car.id, car])); // Map for easier search by id

// calculate generated rentals prices
const rentals = data.rentals.map(({id, car_id, start_date, end_date, distance}) => {
	const days = differenceInDays(Date.parse(end_date), Date.parse(start_date)) + 1;
	const car = cars.get(car_id);
	return {id, price: car.price_per_day * days + car.price_per_km * distance};
});

assert.deepEqual({rentals}, output);
