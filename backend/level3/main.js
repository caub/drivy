const assert = require('assert');
const differenceInDays = require('date-fns/difference_in_days'); // to take care of daylight switches
const data = require('./data.json');
const output = require('./output.json');

const cars = new Map(data.cars.map(car => [car.id, car])); // Map for easier search by id

const pricePerDayRules = [
	{from: 0, to: 1, factor: 1},
	{from: 1, to: 4, factor: .9},
	{from: 4, to: 10, factor: .7},
	{from: 10, to: Infinity, factor: .5}
];

// calculate generated rentals prices
const rentals = data.rentals.map(({id, car_id, start_date, end_date, distance}) => {
	const days = differenceInDays(Date.parse(end_date), Date.parse(start_date)) + 1;
	const car = cars.get(car_id);

	// compute rental duration price part, using pricing rules 
	const durationPrice = pricePerDayRules.reduce((total, {from, to, factor}) => {
		return total + Math.min(to - from, Math.max(0, days - from)) * car.price_per_day * factor
	}, 0);

	const price = durationPrice + car.price_per_km * distance;
	const commissionFee = .3 * price;
	const insuranceFee = commissionFee / 2;
	const assistanceFee = 100 * days;


	return {
		id, 
		price, 
		commission: {
			insurance_fee: insuranceFee,
			assistance_fee: assistanceFee,
			drivy_fee: commissionFee - insuranceFee - assistanceFee
		}
	};
});

assert.deepEqual({rentals}, output);
