const assert = require('assert');
const data = require('./data.json');
const output = require('./output.json');
const computeRentals = require('../compute-rentals');

const cars = new Map(data.cars.map(car => [car.id, car])); // Map for easier search by id

// calculate generated rentals prices
const rentals = computeRentals(cars, data.rentals).map(({id, actions}) => ({id, actions})); // ignore the rental_id: undefined

assert.deepEqual({rentals}, output); 
