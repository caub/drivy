const assert = require('assert');
const data = require('./data.json');
const output = require('./output.json');
const computeRentals = require('../compute-rentals');

const carsMap = new Map(data.cars.map(car => [car.id, car])); // Map for easier search by id
const rentalsMap = new Map(data.rentals.map(rental => [rental.id, rental]));

// calculate generated rentals prices
const rentalsResultMap = new Map(computeRentals(carsMap, data.rentals).map(r => [r.id, r])); // ♥ Map

const patchedRentals = data.rental_modifications.map(r => { // could do ({id, rental_id, ...r}) to be cleaner
	const rental = rentalsMap.get(r.rental_id);
	return Object.assign({}, rental, r); // apply patch;
});

const newRentals = computeRentals(carsMap, patchedRentals);

const deltaRentals = newRentals.map(r => {
	const prevRental = rentalsResultMap.get(r.rental_id);
	const prevActions = new Map(prevRental.actions.map(a => [a.who, a]));
	return {
		id: r.id,
		rental_id: r.rental_id,
		actions: r.actions.map(({who, type, amount}) => {
			const prevAction = prevActions.get(who);
			const deltaAmount = amount - prevAction.amount;
			return {
				who,
				type: deltaAmount < 0 ? (prevAction.type === 'debit' ? 'credit'  :'debit') : prevAction.type,
				amount: Math.abs(deltaAmount)
			};
		})
	};
});

assert.deepEqual({rental_modifications: deltaRentals}, output);
