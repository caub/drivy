// final form
const differenceInDays = require('date-fns/difference_in_days'); // to take care of daylight switches
const pricePerDayRules = require('./price-per-day-rules.json');

module.exports = (carsMap, rentals) => rentals.map(({id, rental_id, car_id, start_date, end_date, distance, deductible_reduction}) => {
	const days = differenceInDays(Date.parse(end_date), Date.parse(start_date)) + 1;
	const car = carsMap.get(car_id);
	if (!car) throw new Error(`missing car id ${car_id}`);

	// compute rental duration price part, using pricing rules 
	const durationPrice = pricePerDayRules.reduce((total, {from, to=Infinity, factor}) => {
		return total + Math.min(to - from, Math.max(0, days - from)) * car.price_per_day * factor
	}, 0);

	const price = durationPrice + car.price_per_km * distance;
	const commissionFee = .3 * price;
	const insuranceFee = commissionFee / 2;
	const assistanceFee = 100 * days;
	const drivyFee = commissionFee - insuranceFee - assistanceFee;
	const deductibleReduction = deductible_reduction ? 400 * days : 0;

	return {
		id,
		rental_id,
		actions: [
			{
				who: 'driver',
				type: 'debit',
				amount: price + deductibleReduction
			},
			{
				who: 'owner',
				type: 'credit',
				amount: price - commissionFee
			},
			{
				who: 'insurance',
				type: 'credit',
				amount: insuranceFee
			},
			{
				who: 'assistance',
				type: 'credit',
				amount: assistanceFee
			},
			{
				who: 'drivy',
				type: 'credit',
				amount: drivyFee + deductibleReduction
			}
		]
	};
});
